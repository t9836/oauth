package main

import (
	"encoding/json"
	"log"
	"os"
	"time"
	docs "tm1-simple-goauth/docs"

	swaggerfiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/go-redis/redis"
)

func getEnv(key, defaultValue string) string {
	value := os.Getenv(key)
	if value == "" {
		return defaultValue
	}
	return value
}

func prepare(client redis.Client) {
	// unicornPass := hashPassword("coba123")
	// clientSecret := hashPassword("1231")
	unicornPass := "coba123"
	clientSecret := "1231"
	newUser := User{
		Username:     "unicorn",
		Password:     unicornPass,
		Fullname:     "Budi Anduk",
		Role:         "seller",
		ClientId:     "7162",
		ClientSecret: clientSecret,
	}
	user, err := json.Marshal(newUser)
	if err != nil {
		log.Println(err)
	}

	err = client.Set(newUser.Username, user, time.Duration(UserExpire)).Err()
	if err != nil {
		log.Println(err)
	}

}

var tokenDB *redis.Client
var userDB *redis.Client

func initDB() (*redis.Client, *redis.Client) {
	activeDB := getEnv("REDIS_URL", "localhost:6379")

	if activeDB == "redis-simple-go-auth-cont:6379" {
		tokenDB = redis.NewClient(&redis.Options{
			Addr:     activeDB,
			Password: getEnv("REDIS_PASSWORD", ""),
			DB:       0,
		})

		userDB = redis.NewClient(&redis.Options{
			Addr:     activeDB,
			Password: getEnv("REDIS_PASSWORD", ""),
			DB:       1,
		})
		return tokenDB, userDB
	} else {
		opt, errDB := redis.ParseURL(activeDB)
		if errDB != nil {
			panic(errDB)
		}
		tokenDB = redis.NewClient(opt)
		userDB = redis.NewClient(opt)
		return tokenDB, userDB
	}
}

func main() {
	tokenDB, userDB := initDB()
	_, err := tokenDB.Ping().Result()
	if err != nil {
		log.Fatal(err)
	}

	_, err = userDB.Ping().Result()
	if err != nil {
		log.Fatal(err)
	}

	prepare(*userDB)

	router := gin.New()
	router.SetTrustedProxies([]string{
		"localhost",
		"infralabs.cs.ui.ac.id",
		"herokuapp.com",
	})
	router.Use(cors.New(cors.Config{
		AllowOrigins: []string{
			"http://localhost:3000",
			"https://fe-kuymakan.vercel.app",
			"https://kuymakan.vercel.app",
			"https://account-a7law.herokuapp.com/",
		},
		AllowMethods:     []string{"GET", "POST"},
		AllowHeaders:     []string{"Origin"},
		ExposeHeaders:    []string{"Content-Length"},
		AllowCredentials: true,
		MaxAge:           12 * time.Hour,
	}))

	// Simple group: OAuth
	oauth := router.Group("/oauth")
	{
		oauth.POST("/token", createTokenHandler)
		oauth.POST("/token/refresh", refreshTokenHandler)
		oauth.POST("/resource", getResourceHandler)
		oauth.POST("/user/register", createUserHandler)
		oauth.PATCH("/user/update/:usernameparam", updateUserHandler)
		oauth.POST("/config", configHandler)
	}

	docs.SwaggerInfo.BasePath = "/oauth"
	router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerfiles.Handler))
	port := getEnv("PORT", "8080")
	router.Run(":" + port)
}
