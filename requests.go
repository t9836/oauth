package main

type createTokenRequest struct {
	Username     string `json:"username"`
	Password     string `json:"password"`
	GrantType    string `json:"grant_type"`
	ClientId     string `json:"client_id"`
	ClientSecret string `json:"client_secret"`
}

type createUserRequest struct {
	Username     string `json:"username"`
	Password     string `json:"password"`
	Fullname     string `json:"full_name"`
	Role         string `json:"role"`
	ClientId     string `json:"client_id"`
	ClientSecret string `json:"client_secret"`
}

type updateUserRequest struct {
	Username string `json:"username"`
	Password string `json:"password"`
	Fullname string `json:"full_name"`
}

type configRequest struct {
	AccessTokenExpire  string `json:"access_token_expire"`
	RefreshTokenExpire string `json:"refresh_token_expire"`
}
