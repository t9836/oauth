package main

import (
	"time"

	"github.com/gin-gonic/gin"
)

func tokenSuccessResponseConstructor(c *gin.Context, newToken TokenInfo) {
	c.JSON(httpStatusOk, gin.H{
		"access_token":  newToken.AccessToken,
		"expires_in":    int(newToken.AccessTokenExpireAt.Sub(time.Now()).Seconds()),
		"token_type":    TokenType,
		"scope":         nil,
		"refresh_token": newToken.RefreshToken,
	})
}

func failedResponseConstructor(c *gin.Context, err string) {
	c.JSON(httpStatusErrReq, gin.H{
		"error":             ErrInvalidReq,
		"error_description": err,
	})
}

func resourceSuccessResponseConstructor(c *gin.Context, tokenInfo TokenInfo, userInfo User) {
	c.JSON(httpStatusOk, gin.H{
		"access_token":  tokenInfo.AccessToken,
		"client_id":     tokenInfo.ClientId,
		"user_id":       userInfo.Username,
		"full_name":     userInfo.Fullname,
		"role":          userInfo.Role,
		"expires":       int(tokenInfo.AccessTokenExpireAt.Sub(time.Now()).Seconds()),
		"refresh_token": tokenInfo.RefreshToken,
	})
}

func configSuccessResponseConstructor(c *gin.Context, conf configRequest) {
	c.JSON(httpStatusOk, gin.H{
		"access_token_expire":  conf.AccessTokenExpire,
		"refresh_token_expire": conf.RefreshTokenExpire,
	})
}

func userUpdateSuccessResponseConstructor(c *gin.Context, updatedUser User) {
	c.JSON(httpStatusOk, gin.H{
		"username":  updatedUser.Username,
		"full_name": updatedUser.Fullname,
		"password":  updatedUser.Password,
	})
}
